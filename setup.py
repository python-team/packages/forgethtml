#!/usr/bin/env python

from distutils.core import setup
setup(name="forgetHTML",
      version="0.8",
      author="Stian Soiland",
      author_email="stian@soiland.no",
      url="http://forgethtml.sourceforge.net/",
      license="LGPL",
      description=
"""forgetHTML is a Python module for writing HTML by building a tree from
different classes, one pr. HTML element. This makes it possible to
modify and create elements in a different order than they will end up in
the final HTML output. """,
      py_modules=['forgetHTML'],
      package_dir = {'': 'lib'}
     )
